'''
Created on Oct 27, 2010

@author: Peter
'''
from numpy import *
from Marchines import kNN
import matplotlib
import matplotlib.pyplot as plt
fig = plt.figure()
ax = fig.add_subplot(111)
# datingDataMat,datingLabels = kNN.file2matrix('datingTestSet2.txt')
# #ax.scatter(datingDataMat[:,1], datingDataMat[:,2])
# ax.scatter(datingDataMat[:,1], datingDataMat[:,2], 15.0*array(datingLabels), 15.0*array(datingLabels))
# ax.axis([-2,25,-0.2,2.0])
da,lab = kNN.file2matrix('datingTestSet3.txt',',')
ax.scatter(da[:,1],da[:,2],15.0*array(lab),15.0*array(lab))

plt.xlabel('Percentage of Time Spent Playing Video Games(游戏时间占比)')
plt.ylabel('Liters of Ice Cream Consumed Per Week')
plt.show()

from Marchines import CART
if __name__ == "__main__":
    # # 测试数据集
    # testMat = mat(eye(4))
    # print testMat
    # print type(testMat)
    # mat0, mat1 = binSplitDataSet(testMat, 1, 0.5)
    # print mat0, '\n-----------\n', mat1

    # # 回归树
    # myDat = loadDataSet('input/9.RegTrees/data1.txt')
    # # myDat = loadDataSet('input/9.RegTrees/data2.txt')
    # # print 'myDat=', myDat
    # myMat = mat(myDat)
    # # print 'myMat=',  myMat
    # myTree = createTree(myMat)
    # print myTree

    # # 1. 预剪枝就是：提起设置最大误差数和最少元素数
    # myDat = loadDataSet('input/9.RegTrees/data3.txt')
    # myMat = mat(myDat)
    # myTree = createTree(myMat, ops=(0, 1))
    # print myTree

    # # 2. 后剪枝就是：通过测试数据，对预测模型进行合并判断
    # myDatTest = loadDataSet('input/9.RegTrees/data3test.txt')
    # myMat2Test = mat(myDatTest)
    # myFinalTree = prune(myTree, myMat2Test)
    # print '\n\n\n-------------------'
    # print myFinalTree

    # # --------
    # # 模型树求解
    # myDat = loadDataSet('input/9.RegTrees/data4.txt')
    # myMat = mat(myDat)
    # myTree = createTree(myMat, modelLeaf, modelErr)
    # print myTree

    # # 回归树 VS 模型树 VS 线性回归
    trainMat = regTrees.mat(regTrees.loadDataSet('bikeSpeedVsIq_train.txt'))
    testMat = regTrees.mat(regTrees.loadDataSet('bikeSpeedVsIq_test.txt'))
    # # 回归树
    myTree1 = regTrees.createTree(trainMat, ops=(1, 20))
    print(myTree1)
    yHat1 = regTrees.createForeCast(myTree1, testMat[:, 0])
    print("--------------\n")
    # print yHat1
    # print "ssss==>", testMat[:, 1]
    print("回归树:", regTrees.corrcoef(yHat1, testMat[:, 1],rowvar=0)[0, 1])

    # 模型树
    myTree2 = regTrees.createTree(trainMat, regTrees.modelLeaf, regTrees.modelErr, ops=(1, 20))
    yHat2 = regTrees.createForeCast(myTree2, testMat[:, 0], regTrees.modelTreeEval)
    print(myTree2)
    print("模型树:", regTrees.corrcoef(yHat2, testMat[:, 1],rowvar=0)[0, 1])

    # 线性回归
    ws, X, Y = regTrees.linearSolve(trainMat)
    print(ws)
    m = len(testMat[:, 0])
    yHat3 = regTrees.mat(regTrees.zeros((m, 1)))
    for i in range(shape(testMat)[0]):
        yHat3[i] = testMat[i, 0]*ws[1, 0] + ws[0, 0]
    print("线性回归:", regTrees.corrcoef(yHat3, testMat[:, 1],rowvar=0)[0, 1])
